//API
let saleData = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=points-de-ventes-titres-de-transport-du-stac&rows=200&facet=libelle&facet=commune";

//Markers group
var markers = L.layerGroup([]).addTo(myMap);

//Icon style
var greenIcon = L.icon({
  iconUrl: 'img/green.png',

  iconSize: [40, 40], // size of the icon
  shadowSize: [50, 64], // size of the shadow
  iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62], // the same for the shadow
  popupAnchor: [-3, -76]
})

//CHECKBOX
function salePoints() {
  // Get the checkbox
  var checkBox = document.getElementById("myCheck");

  // Get the output code
  // If the checkbox is checked, display this code
  if (checkBox.checked === true) {
    fetch(saleData)
      //Code for handling the data from the API
      .then(resp => {
        //In this case return a JSON
        return resp.json();
      })
      .then(response => {
        //Loop on each records
        for (let i = 0; i < response.records.length; i++) {
          //localisation marker with a pop up when the user clicks on the marker
          L.marker(response.records[i].fields.localisation, {
              icon: greenIcon
            })
            .bindPopup(response.records[i].fields.libelle + '</br>' + response.records[i].fields.adresse + '</br>' + response.records[i].fields.code_postal)
            .addTo(markers);
        };
      })
    //remove markers from markers group
  } else {
    markers.clearLayers();
  }
}


