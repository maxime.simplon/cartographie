//API
let busData = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_shape&rows=200&facet=code_ligne&facet=commune&facet=nom";

//Markers group
var markers2 = L.layerGroup([]).addTo(myMap);

//CHECKBOX
function busStop() {
  // Get the checkbox
  var checkBox = document.getElementById("myCheck2");
  // Get the output code
  // If the checkbox is checked, display the output code
  if (checkBox.checked === true) {
    fetch(busData)
      //Code for handling the data from the API
      .then(resp => {
        //In this case return a JSON
        return resp.json();
      })
      .then(response => {
        //Loop on each records
        for (let i = 0; i < response.records.length; i++) {
          //localisation marker with a pop up when the user clicks on the marker
          L.marker(response.records[i].fields.geo_point_2d)
            .bindPopup(response.records[i].fields.commune + '</br>' + response.records[i].fields.nom + '</br>' + response.records[i].fields.code_ligne)
            .addTo(markers2);
        };
      })
    //remove markers from markers group
  } else {
    markers2.clearLayers();
  }
}